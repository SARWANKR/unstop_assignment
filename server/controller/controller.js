const express = require("express");
const trainModel = require("../Model/train");
const mongoose = require("mongoose");

module.exports = {
  bookTicket: async (req, res) => {
    var no_of_seats = req.body.no_of_seats;
    try {
      // Check the Train Model for Data
      var check_booking = await trainModel.findOne({});

      // Create the Booking If Nothing is present in the database
      if (!check_booking) {
        var create_booking = new trainModel({
          bookedSeat: no_of_seats,
        });
        create_booking.save();
        return res.send({
          code: 200,
          status: true,
          response: create_booking,
          message: "Seat Booking Confirmed",
        });
      }
      let difference = check_booking.totalSeats - check_booking.bookedSeat;

      if (difference !== 0) {
        if (difference >= req.body.no_of_seats) {
          // Updating the database if the data is present.
          var update_booking = await trainModel.findOneAndUpdate(
            { _id: check_booking._id },
            {
              bookedSeat: check_booking.bookedSeat + no_of_seats,
            },
            { new: true, lean: true, upsert: true }
          );

          res.send({
            code: 200,
            status: true,
            response: update_booking,
          });
        } else {
          res.send({
            status: false,
            code: 201,
            message: `Max seats availabe = ${diff}`,
          });
        }
      } else {
        res.send({
          code: 201,
          status: false,
          message: "Booking Full",
        });
      }
    } catch (e) {
      return res.send({
        code: 403,
        status: false,
        message: "Something Went Wrong",
      });
    }
  },

  getBooking : async(req , res) => {
    try {

      var check_booking = await trainModel.findOne({});
      if(check_booking){
        check_booking.total_booked_seat = check_booking.bookedSeat;
        res.send({
          code : 200,
          status : true,
          response : check_booking.total_booked_seat,
          message : "Booked Seat"
        })
      }
      else {
        res.send({
          code : 200,
          status : false,
          response : 0,
          message : "No booking Found"
        })
      }


    }
    catch (e) {
      return res.send({
        code: 403,
        status: false,
        message: "Something Went Wrong",
      });
    }

  }
};
