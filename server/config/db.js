require("dotenv").config();
const mongoose = require("mongoose").mongoose;

// DB URI from Environment Variable
const URI = process.env.DB_URI;

// DB Connection

mongoose
  .connect(URI)
  .then(() => {
    console.log("Database connected successfully");
  })
  .catch((err) => {
    console.log("Database could not be connected");
  });
