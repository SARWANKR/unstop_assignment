const mongoose = require("mongoose");
const schema = mongoose.Schema;

const trainSchema = new schema({
  trainName: { type: String, default: "Vandhe Bharat Express" },
  coachNo: { type: String, default: "S1" },
  totalSeats: { type: Number, default: 80 },
  bookedSeat: { type: Number, default: 0 },
});

const trainModel = mongoose.model("trainSchema", trainSchema);
module.exports = trainModel;
