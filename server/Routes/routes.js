const express = require("express");
const Joi = require("joi");
const router = express.Router();
const controller = require("../controller/controller");

// Route for the Booking
router.post("/user/bookTicket", joiValidation, controller.bookTicket);
router.get("/user/getbooking" , controller.getBooking)

// Joi Validation For the Router
function joiValidation(req, res, next) {
  const bookingSchema = Joi.object({
    no_of_seats: Joi.number().integer().min(1).max(7).messages({
      "number.base": "Invalid number. Please provide a valid number.",
      "number.integer": "Invalid number. Please provide an integer.",
      "number.min": "Number must be greater than or equal to 1.",
      "number.max": "Number must be less than or equal to 7.",
    }),
  });

  // schema options
  const options = {
    abortEarly: false, // include all errors
    allowUnknown: true, // ignore unknown props
    stripUnknown: true, // remove unknown props
  };

  // Path for the Joi validation
  if (req.path == "/user/bookTicket")
    var { error, value } = bookingSchema.validate(req.body, options);

  if (error) {
    // returning the error if there is anything
    return res.json({
      status: false,
      code: 201,
      message: `${error.details.map((x) => x.message.replace(/"/g, ""))[0]}`,
    });
  } else {
    req.body = value;
    next();
  }
}

module.exports = router;
