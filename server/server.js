const express = require("express");
require("dotenv").config();
const cors = require("cors");
const userRoutes = require("./Routes/routes");
const cluster = require("node:cluster");
const http = require("node:http");
const numCPUs = require("node:os").cpus().length;
const process = require("node:process");

// Port are taken from .env file
const PORT = process.env.PORT || 3100;

// Connect to the database

const db = require("./config/db");

// Cluster to use each core of the CPU
if (cluster.isPrimary) {
  console.log(`Primary ${process.pid} is running`);

  // Fork workers.
  for (let i = 0; i < numCPUs; i++) {
    cluster.fork();
  }

  cluster.on("exit", (worker, code, signal) => {
    console.log(`worker ${worker.process.pid} died`);
  });
} else {
  // Workers can share any TCP connection
  // In this case it is an HTTP server

  const app = express();

  // Middlewares
  app.use(express.urlencoded({ extended: true }));
  app.use(express.json());
  app.use(express.json({ type: "application/vnd.api+json" }));
  app.use(cors());
  app.use(userRoutes);


  app.listen(PORT, () => {
    console.log(
      "Server is running on port " + PORT + " - Worker " + cluster.worker.id
    );
  });
}
